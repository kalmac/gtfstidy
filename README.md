# Usage

```bash
docker run -it --rm -v $(pwd):/app kalmac/gtfstidy -h
```

All files in current directory are accessible to `gtfstidy`.

To know more about `gtfstidy`, see the
[Github page](https://github.com/patrickbr/gtfstidy)
