FROM golang:1.15-alpine AS builder

RUN apk add --no-cache git
RUN git clone https://github.com/patrickbr/gtfstidy.git; \
    cd gtfstidy; \
    git checkout tags/v0.2
RUN go get github.com/patrickbr/gtfstidy

FROM alpine
COPY --from=builder /go/bin/gtfstidy /usr/local/bin/gtfstidy
COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
